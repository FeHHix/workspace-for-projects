const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const common = require('./common.config.js');

const ASSET_PATH = process.env.ASSET_PATH || '/';

const PATHS = {
    app: path.resolve(__dirname, '../src/index.js'),
    assets: path.resolve(__dirname, '../src/assets'),
    build: path.resolve(__dirname, '../build/dist')
};

module.exports = merge(common, {
    devtool: '',

    entry: [
        PATHS.app
    ],

    output: {
        path: PATHS.build,
        publicPath: ASSET_PATH
    },

    plugins: [
        new CleanWebpackPlugin(['build/dist'], {root: path.resolve(__dirname, '../')}),
        new UglifyJSPlugin({
            sourceMap: true,
            uglifyOptions: {
                ecma: 6,
                extractComments: true
            }
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'ASSET_PATH': JSON.stringify(ASSET_PATH),
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.css$/,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {discardComments: {removeAll: true}},
            canPrint: true
        }),
        new CopyWebpackPlugin([], {
            copyUnmodified: true
        })
    ]
});
