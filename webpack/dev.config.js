const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./common.config.js');

const PATHS = {
    app: path.resolve(__dirname, '../src/index.js'),
    source: path.resolve(__dirname, '../src')
};

module.exports = merge(common, {
    devtool: 'eval',

    entry: [
        'react-hot-loader/patch',
        PATHS.app
    ],

    devServer: {
        contentBase: PATHS.source,
        hot: true
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ]
});
