const path = require('path');
const marked = require("marked");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const renderer = new marked.Renderer();

const PATHS = {
    assets: path.resolve(__dirname, '../src/assets'),
    source: path.resolve(__dirname, '../src')
};

module.exports = {
    context: PATHS.source,

    output: {
        filename: 'bundle.js',
        publicPath: '/'
    },

    resolve: {
        modules: [PATHS.source, 'node_modules'],
        extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.json']
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Alien Universe Web Site',
            template: PATHS.assets + '/index.html'
        }),
        new ExtractTextPlugin({
            filename: 'bundle.css',
            disable: false,
            allChunks: true
        })
    ],

    module: {
        rules: [
            {
                test: /bootstrap-sass\/assets\/javascripts\//,
                loader: 'imports?jQuery=jquery'
            },
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff2'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.otf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-otf'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
            },
            {
                test: /\.scss$/, 
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader', 'sass-loader']
                })
            },
            {
                test: /\.ts|.tsx$/,
                loaders: ['react-hot-loader/webpack', 'ts-loader']
            },
            {
                test: /\.md$/,
                use: [
                    {
                        loader: "html-loader"
                    },
                    {
                        loader: "markdown-loader",
                        options: {
                            pedantic: true,
                            renderer
                        }
                    }
                ]
            }
        ]
    }
};
