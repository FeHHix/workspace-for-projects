# Готовый workspace для разработки React-проектов

Позволяет быстро начать разработку проектов на React, Redux и TypeScript.
Включает в себя Webpack с настроенным dev server и конфигами, Typescript, в том числе, как транспайлер, и Express-фреймворк с настроенным http-сервером для тестирования сборок.

### Особенности
***

* Webpack с настроенным dev server, HMR и отдельными конфигами для common+dev и common+prod сборок
* TypeScript 2.6 как основной язык разработки и как транспайлер TS->JS ES5
* Препроцессор SASS с необходимыми лоадерами и настроенный ExtractTextPlugin для упаковки css в один файл
* Bootstrap, в том числе зависимости для поддержки Bootstrap-компонентов в React
* HtmlWebpackPlugin для генерации готового index.html из шаблона
* Бета версия UglifyJSPlugin c поддержкой минификации JS ES6
* OptimizeCssAssetsPlugin для минификации и оптимизации css bundle
* Express-фреймворк с настроенным http-сервером для тестирования production сборок

### Ключевые зависимости
***

* React 15
* React Router 3
* Redux
* Bootstrap 3
* React Bootstrap и React Router Bootstrap. **Внимание! На данный момент не имеют полной поддержки React 16**

### Team

@FeHHix

### Version

1.0.0
